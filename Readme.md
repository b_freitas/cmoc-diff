#### Instructions

1. Fork this project
2. Create a [Personal Access Token (PAT)](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with the `api` scope
3. In the newly forked project, create a [CI/CD variable](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project) with the name `GITLAB_API_PRIVATE_TOKEN` and your PAT as the value
4. Run the pipeline manually
5. The changes should appear in this `Readme.md` file, in the section below
6. [Optional] You can also specify a date you want the search to start from. To do so, set a variable called `OVERRIDE_DATE` before running pipeline with the date in the following format: `dd/mm/yyyy`

#### CMOC Info

- Name: Bruno Freitas
- Current date: 2025-03-09 17:48:00 +0000
- Last day of my previous rotation: 2024-12-22T15:51:17.351Z
- Workflow changes (commits) since my previous CMOC rotation: 4
- Current workflow file: [cmoc_workflows.html.md](https://gitlab.com/gitlab-com/content-sites/handbook/-/raw/aad639bb352808a26d71758063991c3e6f2ea56d/content/handbook/support/workflows/cmoc_workflows.md)
- Workflow file from my previous CMOC shift: [cmoc_workflows.html.md](https://gitlab.com/gitlab-com/content-sites/handbook/-/raw/d716f7eb5ec00c57b25cad242e45948fe7b520b6/content/handbook/support/workflows/cmoc_workflows.md)

---

#### Differences in the workflow between my previous CMOC rotation and now

```diff
--- cmoc_workflows_previous.html.md	2025-03-09 17:47:57.202277513 +0000
+++ cmoc_workflows_latest.html.md	2025-03-09 17:47:58.948277770 +0000
@@ -109,11 +109,17 @@
 
 ### About Contact Requests
 
-Whether related to an ongoing incident or not, Infrastructure or Security may ask you to reach out to one or more users if they detect unusual usage. Please follow the [Sending Notices]({{< ref "sending_notices" >}}) workflow to action these requests. Additionally, refer to the [End of Shift Handover Procedure](#end-of-shift-handover-procedure) for details on handing off contact requests.
+Whether related to an ongoing incident or not, Infrastructure or Security may ask you to reach out to one or more users if they detect unusual usage. Please follow the [Sending Notices](/handbook/support/workflows/sending_notices/) workflow to action these requests. Additionally, refer to the [End of Shift Handover Procedure](#end-of-shift-handover-procedure) for details on handing off contact requests.
 
 ### How to Page the CMOC?
 
-The CMOC can be paged during the [incident declaration process](/handbook/engineering/infrastructure/incident-management/#reporting-an-incident). If the CMOC needs to be paged after an incident was created or for any other reason, see the [How to engage the CMOC?](/handbook/engineering/infrastructure/incident-management/#how-to-engage-the-cmoc) section of the main incident management handbook.
+The CMOC can be paged during the [incident declaration process](/handbook/engineering/infrastructure/incident-management/#reporting-an-incident). If the CMOC needs to be paged after an incident was created or for any other reason, see the [How to engage the EOC, IM or CMOC?](/handbook/engineering/infrastructure/incident-management/#how-to-engage-the-eoc-im-or-cmoc) section of the main incident management handbook.
+
+### About Coordinating a Support Response
+
+When there is an incident that results in unexpected customer impact, and requires a non-standard workflow or communication from Support, you should create a [Support Response](https://gitlab.com/gitlab-com/support/support-team-meta/-/blob/master/.gitlab/issue_templates/Support%20Response.md) issue to help coordinate Support action and response.
+
+Keep this issue updated as communication guidelines or workflows change so that it remains the single source of truth for Support-related information.
 
 ### CMOC Performance Indicators
 
@@ -445,7 +451,7 @@
 > **Note About Automated Maintenance Events**: On the Maintenance Event page you may see `Automation: Running`  with red text in parenthesis next to it reading `(Disable)`.
 Once `(Disable)` has been clicked and subsequently disabled it cannot be re-enabled.
 In order to `Post Update` and `Finish Maintenance` the automated Maintenance Event must be `(Disable)`.
-After being disabled all future updates to this Maintenance Event must be manual updates from that point forward.
+After being disabled all future updates, including starting the Maintenance Event must be performed manually from that point forward.
 
 To send an update about a maintenance event, such as a reminder, go to the *Maintenances* tab in Status.io and select the one that needs an update. On the maintenance's information page, make note of whether automatic email reminders are set to go out. If yes, make sure not to send email broadcasts for your update in order to avoid sending duplicate reminders to subscribers. Once ready to update, select the *Post Update Without Starting* button.
 

```