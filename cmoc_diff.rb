gem 'gitlab'

require 'gitlab'
require 'open-uri'

class CMOCDiff

  #TODO Add a variable to allow users to manually specify the last CMOC rotation

  def author_username
    ENV['GITLAB_USER_LOGIN']
  end

  def author_name
    ENV['GITLAB_USER_NAME']
  end

  def default_branch
    ENV['CI_DEFAULT_BRANCH']
  end

  def current_project_id
    ENV['CI_PROJECT_ID']
  end

  def override_date
    if ENV['OVERRIDE_DATE']
      DateTime.parse(ENV['OVERRIDE_DATE'])
    end
  end

  def reference_date
    override_date.nil? ? last_cmoc_rotation_date : override_date
  end


  GITLAB_URL = "https://gitlab.com"
  GITLAB_API_ENDPOINT = "https://gitlab.com/api/v4"
  CMOC_ROTATION_PROJECT_NAME = "gitlab-com/support/dotcom/cmoc-handover"
  CMOC_WORKFLOW_PROJECT_NAME = "gitlab-com/content-sites/handbook"
  CMOC_WORKFLOW_FILE_PATH = "content/handbook/support/workflows/cmoc_workflows.md"
  CMOC_WORKFLOW_PROJECT_ID = 42817607
  PREVIOUS_CMOC_WORKFLOW_FILENAME = "cmoc_workflows_previous.html.md"
  LATEST_CMOC_WORKFLOW_FILENAME = "cmoc_workflows_latest.html.md"
  DIFF_OUTPUT_FILENAME = "Readme.md"
  DEFAULT_COMMITS_SEARCH_ARS = {
    path: CMOC_WORKFLOW_FILE_PATH,
    page: 1,
    per_page: 1
  }.freeze
  DEFAULT_ISSUES_SEARCH_ARGS = {
    order_by: "created_at",
    sort: "desc"
  }.freeze

  #TODO Printout each step of the script

  Gitlab.configure do |config|
    config.endpoint = GITLAB_API_ENDPOINT # API endpoint URL, default: ENV['GITLAB_API_ENDPOINT'] and falls back to ENV['CI_API_V4_URL']
    config.private_token = ENV['GITLAB_API_PRIVATE_TOKEN']
  end

  def get_issues(search_args = {})
    search_args.merge(DEFAULT_ISSUES_SEARCH_ARGS)
    Gitlab.issues(CMOC_ROTATION_PROJECT_NAME, search_args)
  end

  def last_cmoc_rotation_date
    get_issues(author_username: author_username).first.created_at
  end

  def get_commits(search_args = {})
    search_args.merge(DEFAULT_COMMITS_SEARCH_ARS)
    Gitlab.commits(CMOC_WORKFLOW_PROJECT_NAME, search_args)
  end

  def latest_commit
    get_commits(since: reference_date).first
  end

  def previous_commit
    get_commits(until: reference_date).first
  end

  def cmoc_workflow_file_url(commit_id)
    "#{GITLAB_URL}/#{CMOC_WORKFLOW_PROJECT_NAME}/-/raw/#{commit_id}/#{CMOC_WORKFLOW_FILE_PATH}"
  end

  def latest_cmoc_workflow_file_url
    cmoc_workflow_file_url(latest_commit.id)
  end

  def previous_cmoc_workflow_file_url
    cmoc_workflow_file_url(previous_commit.id)
  end

  def download_file(source,destination)
    open(destination, 'wb') do |file|
      file << URI.open(source).read
    end
  end

  def diff_files(file1, file2)
    diff_content = "```diff\n"
    diff_content += `diff -u #{file1} #{file2}`
    diff_content += "\n```"
    diff_content
  end

  def diff_output_commit_args
    {
      since: reference_date,
      path: CMOC_WORKFLOW_FILE_PATH,
      page: 1,
      per_page: 50
    }
  end

  def diff_output
    output = "#### Instructions\n\n"
    output += "1. Fork this project\n"
    output += "2. Create a [Personal Access Token (PAT)](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with the `api` scope\n"
    output += "3. In the newly forked project, create a [CI/CD variable](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project) with the name `GITLAB_API_PRIVATE_TOKEN` and your PAT as the value\n"
    output += "4. Run the pipeline manually\n"
    output += "5. The changes should appear in this `Readme.md` file, in the section below\n"
    output += "6. [Optional] You can also specify a date you want the search to start from. To do so, set a variable called `OVERRIDE_DATE` before running pipeline with the date in the following format: `dd/mm/yyyy`\n\n"

    output += "#### CMOC Info\n\n"
    output += "- Name: #{author_name}\n"
    output += "- Current date: #{Time.now}\n"
    output += "- Last day of my previous rotation: #{last_cmoc_rotation_date}\n"
    if override_date
      output += "- Override date: #{reference_date}\n"
      output += "- Workflow changes (commits) since override date: #{get_commits(diff_output_commit_args).count}\n"
      output += "- Current workflow file: [cmoc_workflows.html.md](#{latest_cmoc_workflow_file_url})\n"
      output += "- Workflow file from override date: [cmoc_workflows.html.md](#{previous_cmoc_workflow_file_url})\n\n"
      output += "---\n\n"
      output += "#### Differences in the workflow between override date and now\n\n"
    else
      output += "- Workflow changes (commits) since my previous CMOC rotation: #{get_commits(diff_output_commit_args).count}\n"
      output += "- Current workflow file: [cmoc_workflows.html.md](#{latest_cmoc_workflow_file_url})\n"
      output += "- Workflow file from my previous CMOC shift: [cmoc_workflows.html.md](#{previous_cmoc_workflow_file_url})\n\n"
      output += "---\n\n"
      output += "#### Differences in the workflow between my previous CMOC rotation and now\n\n"
    end
    output + diff_files(PREVIOUS_CMOC_WORKFLOW_FILENAME, LATEST_CMOC_WORKFLOW_FILENAME)
  end

  def override_date_output
    output += "- Override date: #{reference_date}\n"
    output += "- Workflow changes (commits) since override date: #{get_commits(diff_output_commit_args).count}\n"
    output += "- Current workflow file: [cmoc_workflows.html.md](#{latest_cmoc_workflow_file_url})\n"
    output += "- Workflow file from override date: [cmoc_workflows.html.md](#{previous_cmoc_workflow_file_url})\n\n"
    output += "---\n\n"
    output + "#### Differences in the workflow between override date and now\n\n"
  end

  def diff_output_file_exists?
    begin
      Gitlab.get_file(current_project_id, DIFF_OUTPUT_FILENAME, default_branch)
    rescue Gitlab::Error::NotFound => error
      puts error
      puts "Relax, it's all good. I just needed to check if the file was already there. Since it doesn't exist, I'll create a new one!"
      false
    else
      true
    end
  end

  def commit_message(message)
    date = `date`
    # "#{date} | Updating #{DIFF_OUTPUT_FILENAME} with the differences"
    "#{date} | #{message}"
  end

  def remove_diff_output_file
    Gitlab.remove_file(current_project_id, DIFF_OUTPUT_FILENAME, default_branch, commit_message("Removing #{DIFF_OUTPUT_FILENAME}"))
  end

  def create_diff_output_file
    Gitlab.create_file(current_project_id, DIFF_OUTPUT_FILENAME, default_branch, diff_output, commit_message("Creating #{DIFF_OUTPUT_FILENAME}"))
  end

  def commit_diff_output_file
    if(diff_output_file_exists?)
      remove_diff_output_file
    end
    create_diff_output_file
  end

end